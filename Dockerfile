# Development image
FROM python:3.8-slim-buster AS dev

ENV TZ Europe/Berlin

WORKDIR /app
COPY . /app

RUN pip install --upgrade pip wheel
RUN pip wheel --no-deps -w /cache/wheels-dep -r requirements.txt
RUN pip install --no-deps --no-index /cache/wheels-dep/*.whl
RUN pip install --no-deps -r requirements-dev.txt

# Production image
FROM python:3.8-slim-buster

WORKDIR /app
COPY ./usermanager /app/usermanager

COPY --from=dev /cache/wheels-dep /cache/wheels-dep
RUN pip install --no-deps --no-index /cache/wheels-dep/*.whl

EXPOSE 4000
CMD ["python3", "app.py"]
