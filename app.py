import logging
import os
import asyncio
import ssl as ssl_lib
from flask import Flask
import certifi
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from messages import StandupReminder, RandomQuote

# Initialize a Flask app to host the events adapter
app = Flask(__name__)
slack_events_adapter = SlackEventAdapter(
    os.environ["SLACK_SIGNING_SECRET"], "/slack/events", app
)

# Initialize a Web API client
slack_web_client = WebClient(token=os.environ["SLACK_BOT_TOKEN"])


def send_reminder(user_id: str, channel: str):
    reminder = StandupReminder(channel)
    msg = reminder.get_message_payload()
    response = slack_web_client.chat_postMessage(**msg)
    return response


def send_quote(user_id: str, channel: str):
    quote = RandomQuote(channel)
    msg = quote.get_message_payload()
    response = slack_web_client.chat_postMessage(**msg)
    return response


@slack_events_adapter.on(event="app_mention")
def message(payload):
    """
    Reaction after receiving a message that contains "start".
    """
    event = payload.get("event", {})

    channel_id = event.get("channel")
    user_id = event.get("user")
    text = event.get("text")

    logger.info("Got event %s", event)
    if text:
        logger.debug("Event has text %s", text)
        if "quote" in text:
            send_quote(user_id, channel_id)
        else:
            send_reminder(user_id, channel_id)


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    app.run("0.0.0.0", port=443)
