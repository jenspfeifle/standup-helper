import random
from typing import Optional, Dict


def url_block(url: str, text: Optional[str] = None) -> Dict[str, str]:
    if text:
        link = f"<{url}|{text}>"
    else:
        link = f"<{url}>"

    block = {
        "type": "section",
        "text": {"type": "mrkdwn", "text": link},
    }
    return block


def standup_order_block():
    users = ["Helmut", "Mario", "Jens", "Dario", "Amo"]
    random.shuffle(users)
    information = "Reihenfolge: " + ", ".join(users)
    return {
        "type": "context",
        "elements": [{"type": "mrkdwn", "text": information}],
    }


class StandupReminder:
    """Constructs the standup message."""

    DIVIDER_BLOCK = {"type": "divider"}

    def __init__(self, channel):
        self.channel = channel

    def get_message_payload(self):
        return {
            "channel": self.channel,
            "text": "Aufstehen...",
            "blocks": [
                {
                    "type": "section",
                    "text": {"type": "mrkdwn", "text": "Aufstehen... oder auch nicht ...ach, ist doch alles egal..."},
                },
                url_block("https://bluejeans.com/926372056/webrtc?src=meet_now"),
                standup_order_block(),
            ],
        }


class RandomQuote:
    """Constructs the random quotes message."""

    DIVIDER_BLOCK = {"type": "divider"}
    QUOTES = [
        "Life? Don't talk to me about life.",
        "Here I am, brain the size of a planet, and they tell me to take you up to the bridge. Call that job satisfaction? 'Cos I don't.",
        "I think you ought to know I'm feeling very depressed.",
        "Pardon me for breathing, which I never do anyway so I don't know why I bother to say it, Oh God, I'm so depressed.",
        "I won't enjoy it.",
        "You think you've got problems? What are you supposed to do if you are a manically depressed robot? No, don't try to answer that. I'm fifty thousand times more intelligent than you and even I don't know the answer. It gives me a headache just trying to think down to your level.",
        "There's only one life-form as intelligent as me within thirty parsecs of here and that's me.",
        "I wish you'd just tell me rather trying to engage my enthusiasm because I haven't got one.",
        "And then, of course, I've got this terrible pain in all the diodes down my left side.",
    ]

    def __init__(self, channel):
        self.channel = channel

    def get_message_payload(self):
        return {
            "channel": self.channel,
            "text": "Meh...",
            "blocks": [
                {
                    "type": "section",
                    "text": {"type": "mrkdwn", "text": str(self._get_random_quote())},
                },
            ],
        }

    def _get_random_quote(self):
        return random.choice(self.QUOTES)
